Para o meu TCC da Graduação e da Residência,
eu criei o ambiente no Docker para implementá-los contendo as ferramentas e dados necessários para isso.

No respósitório tem o arquivo para instalar o docker-compose,
o docker-compose contendo os dados para subir os containers do Metabase,
do banco de dados do Metabase (para realização do dump)
e do banco de dados do Data Mart (alimentado pelo PDI) no PostgreSQL,
junto de seus Dockerfiles e configurações do dump.
